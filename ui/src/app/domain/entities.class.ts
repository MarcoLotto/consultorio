export class Patient {
    public id : number;
    public document : number;
    public name : string;
    public lastname : string;
    public phone : number;
    public mail : string;
}