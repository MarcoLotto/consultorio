import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import 'hammerjs';

import { MainRoutingModule, routedComponents } from './app.router';

import { AppComponent } from './app.component';
import { PatientSearchComponent } from './components/patient-search-component/patient.search.component';
import { PatientInfoComponent } from './components/patient-info/patient.info.component';
import { PatientService } from './services/patient.srv';
import { ClickOutsideDirective } from './components/click-outside/click.outside.dir';

@NgModule({
  declarations: [
    AppComponent,
    routedComponents,
    PatientSearchComponent,
    PatientInfoComponent,
    ClickOutsideDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MainRoutingModule,
    MaterialModule.forRoot()
  ],
  providers: [PatientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
