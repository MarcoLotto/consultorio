import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Patient } from '../domain/entities.class';

@Injectable()
export class PatientService {

    constructor(private http : Http) { }

    getPatientByDocument(query:string){
        return this.http.get("/api/v1/patient", this.buildSearchParams(query))
                    .map(response => response.json());
    }

    savePatient(patient : Patient){
        return this.http.post("/api/v1/patient", patient, {});
    }

    deletePatient(patient : Patient){
        return this.http.delete("/api/v1/patient/" + patient.id);
    }

    buildSearchParams(query: string) {
        let params = new URLSearchParams();
        params.set('query', query);
        return {search: params};
    }
}