import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PatientService } from '../../services/patient.srv';
import { Patient } from '../../domain/entities.class';

@Component({
    selector: 'patient-search',
    templateUrl: 'patient.search.component.html',
    styleUrls: ['patient.search.component.scss']
})
export class PatientSearchComponent {

    @Output() patientSelected = new EventEmitter<Patient>();

    private query : string;
    private isLoading : boolean;
    private results : Patient[];

    constructor(private patientService: PatientService) {
        this.query = "";
        this.isLoading = false;
        this.results = [];
    }

    onQueryChanged() : void {
        this.results = [];
        if(this.query.length >= 2){
            this.isLoading = true;

            // Hacemos el pedido al servidor para la busqueda de pacientes
            this.patientService.getPatientByDocument(this.query)
                .subscribe(response => {
                    if(this.query.length > 0){
                        this.results = response;
                    }
                    this.isLoading = false;
                }, err => this.isLoading = false);
        }
    }

    closeAutocomplete(){
        this.results = [];
        this.query = "";
    }

    onItemSelected(item){
        this.closeAutocomplete();
        this.patientSelected.emit(item);
    }

    onNewPatientSelected(){
       this.onItemSelected(new Patient()); 
    }
}