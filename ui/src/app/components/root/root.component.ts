import { Component, OnInit } from '@angular/core';
import { Patient } from '../../domain/entities.class';

@Component({
    selector: 'root',
    templateUrl: 'root.component.html',
    styleUrls: ['root.component.scss']
})
export class RootComponent implements OnInit {
    
    private actualPatient : Patient;
    
    constructor() { }

    ngOnInit() { }

    onPatientSelected(patient){
        this.actualPatient = patient;
    }
}