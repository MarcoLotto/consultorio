import { Component, OnInit, Input } from '@angular/core';
import { Patient } from '../../domain/entities.class';
import { PatientService } from '../../services/patient.srv';

@Component({
    selector: 'patient-info',
    templateUrl: 'patient.info.component.html',
    styleUrls: ['patient.info.component.scss']
})
export class PatientInfoComponent {

    @Input() patient : Patient;

    private query : string;
    private isLoading : boolean;
    private onEdition : boolean;

    constructor(private patientService : PatientService) {
        this.query = "";
        this.isLoading = false;
        this.onEdition = false;
    }

    onQueryChanged() : void {
        this.isLoading = (this.query.length >= 2);

        // TODO: Hacer el pedido para la busqueda al servidor
    }

    isOnEdition(){
        return this.onEdition && !this.isLoading;
    }

    save(){
        this.isLoading = true;
        this.patientService.savePatient(this.patient)
                .subscribe(response => {
                    this.isLoading = false;
                    this.onEdition = false;
                }, err => this.isLoading = false);
    }

    ngOnChanges(changes) {
        this.onEdition = !(changes.patient && changes.patient.currentValue.document 
                                           && changes.patient.currentValue.name 
                                           && changes.patient.currentValue.lastname);
    }

    onDelete(){
        this.isLoading = true;
        this.patientService.deletePatient(this.patient)
                .subscribe(response => {
                    this.isLoading = false;
                    this.patient = new Patient();
                }, err => this.isLoading = false);
    }

    isFormValid(){
        return this.patient && this.patient.name && this.patient.lastname && this.patient.document;
    }
}