package application.patient;

import org.springframework.web.bind.annotation.RestController;
import application.domain.Patient;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class PatientController {
	
	private PatientRepository respository;
	
	@Autowired
	public PatientController(PatientRepository respository){
		this.respository = respository;
	}
	
    @RequestMapping(value="/api/v1/patient", method=RequestMethod.GET)
    public List<Patient> get(@RequestParam("query") String name) {
        return this.respository.getByName(name);
    }
    
    @RequestMapping(value="/api/v1/patient", method=RequestMethod.POST)
    public ResponseEntity<String> save(@RequestBody Patient patient){
    	try{
    		this.respository.save(patient);
    		return new ResponseEntity<String>(HttpStatus.CREATED);
    	}
    	catch(DataIntegrityViolationException e){
    		return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
    	}
    }
    
    @RequestMapping(value="/api/v1/patient/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable("id") Long id){
    	this.respository.delete(id);
    	return new ResponseEntity<String>(HttpStatus.OK);
    }
    
    
}
