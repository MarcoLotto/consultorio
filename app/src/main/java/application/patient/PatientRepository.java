package application.patient;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import application.domain.Patient;

public interface PatientRepository extends JpaRepository<Patient, Long> {

	@Query("select p from Patient p where p.name LIKE %?1% OR p.lastname LIKE %?1% OR p.document = ?1")
	public List<Patient> getByName(String name);
	
}
