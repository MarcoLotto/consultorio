git pull

# Backend
cd ./app
chmod +x gradlew
./gradlew build
sudo mv ./build/libs/gs-spring-boot-0.1.0.jar /var/consultorio/consultorio.jar
sudo service consultorio-back stop
sudo service consultorio-back start
cd ../

# Frontend
cd ./ui
npm install
ng build

sudo rm -rf /etc/nginx/www
sudo mv ./dist /etc/nginx/www
sudo cp ./nginx.conf /etc/nginx/nginx.conf

sudo service nginx stop
sudo service nginx start



